from __future__ import annotations
from dataclasses import dataclass
from itertools import cycle, islice

from typing import Tuple, List, Optional
from numpy.typing import ArrayLike

import potpourri3d as pp3d
import os

import numpy as np
import scipy as sp
import scipy.sparse
import scipy.sparse.linalg

import polyscope as ps


def compute_edges(faces: np.ndarray) -> np.ndarray:
    '''compute edge list from faces'''
    edges: List[Tuple[int, int]] = []

    for face in faces:
        for start, end in zip(face, islice(cycle(face), 1, None)):
            edges.append((min(start, end), max(start, end)))

    return np.array(list(edges))


assert np.all(compute_edges(np.array([[0, 1, 2, 3]])) == np.array([[0, 1],
                                                                   [1, 2],
                                                                   [2, 3],
                                                                   [0, 3]]))


def assemble_adjacency(edges: np.ndarray, vertex_count: int) -> sp.sparse.lil_matrix:
    '''compute adjacency matrix from edge list and vertex count'''
    matrix = sp.sparse.lil_matrix((len(edges), vertex_count))
    for k, (i, j) in enumerate(edges):
        matrix[k, i] = -1
        matrix[k, j] = 1

    return matrix


@dataclass
class Mesh:
    name: str
    vertices: np.ndarray
    faces: np.ndarray
    surf: ps.surface_mesh.SurfaceMesh

    @staticmethod
    def load(name: str, filename: str, translation: Optional[ArrayLike] = None) -> Mesh:
        '''load mesh from given filename, with optional translation for display'''
        vertices, faces = pp3d.read_mesh(filename)

        # TODO triangulate quads?
        return Mesh.create(name=name, vertices=vertices, faces=faces, translation=translation)

    @staticmethod
    def create(name: str, vertices: np.ndarray, faces: np.ndarray, translation: Optional[ArrayLike] = None):
        '''create mesh from given vertices/faces, with optional translation for display'''
        surf = ps.register_surface_mesh(name,
                                        vertices if translation is None else vertices + translation,
                                        faces)
        return Mesh(name=name, vertices=vertices, faces=faces, surf=surf)


def expand(matrix: sp.sparse.csr_matrix, size: int=3) -> sp.sparse.lil_matrix:
    '''expand matrix by computing kronecker product with identity(size)'''
    return sp.sparse.kron(matrix, np.identity(size))


def laplacian_matrix(faces: np.ndarray, vertex_count: int) -> sp.sparse.lil_matrix:
    '''compute (umbrella) laplacian matrix from mesh faces and vertex count'''
    edges = compute_edges(faces)
    adjacency = assemble_adjacency(edges, vertex_count=vertex_count)

    result = adjacency.T.dot(adjacency)
    result.eliminate_zeros()
    result.sort_indices()

    return result


def data_path(filename: str) -> str:
    '''convenience function to get paths to data files'''
    return os.path.join(os.path.dirname(__file__), 'data', filename)


def solve_constrained_least_squares(A: sp.sparse.csr_matrix,
                                    b: np.ndarray,
                                    J: sp.sparse.csr_matrix,
                                    c: np.ndarray) -> np.ndarray:
    assert A.shape[1] == J.shape[1]
    assert A.shape[0] == b.shape[0]
    assert len(b.shape) == 1

    assert J.shape[0] == c.shape[0]
    assert len(c.shape) == 1

    m = A.shape[1]
    n = J.shape[0]
    size = m + n

    # KKT system: [[A.T * A, J.T],
    #              [J, 0]] = [A.T * b, c]       
    K = sp.sparse.coo_matrix((size, size))

    top_left = A.T.dot(A).tocoo()
    bottom_left = J.tocoo()

    data = np.concatenate((top_left.data,
                           bottom_left.data,
                           bottom_left.data,))

    row = np.concatenate((top_left.row,
                          bottom_left.row + m,
                          bottom_left.col,
                          ))

    col = np.concatenate((top_left.col,
                          bottom_left.col,
                          bottom_left.row + m))

    K = sp.sparse.coo_matrix((data, (row, col)), shape=(size, size))

    y = np.zeros(size)
    y[:m] = A.T.dot(b)
    y[m:] = c

    # solve system
    x = sp.sparse.linalg.spsolve(K.tocsr(), y)
    
    # return primal part only
    return x[:m]


def assemble_roi_matrix(roi: np.ndarray, vertex_count: int) -> sp.sparse.lil_matrix:
    constraints = sp.sparse.lil_matrix((len(roi), vertex_count))
    for i, index in enumerate(roi):
        constraints[i, index] = 1

    return constraints


# display offset
offset = 0.2

if __name__ == '__main__':
    ps.init()

    # load reference/registered meshes
    reference = Mesh.load('reference',
                          filename=data_path('reference.obj'),
                          translation=[-offset, 0, 0])
    registration = Mesh.load('registration',
                             filename=data_path('registration.obj'),
                             translation=[offset, 0, 0])

    assert np.all(reference.faces == registration.faces), "reference/registration topology error"

    # compute laplacian matrix from reference mesh topology
    delta = laplacian_matrix(faces=reference.faces,
                             vertex_count=len(reference.vertices))

    # expand laplacian matrix to act on 3d coordinates instead of vertex indices
    A = expand(delta, 3)
    assert A.shape[0] == 3 * len(reference.vertices)
    assert A.shape[1] == 3 * len(reference.vertices)

    # compute reference mesh laplacians
    b = A.dot(reference.vertices.flatten())

    # load constraint Region of Interest data (fixed vertices)
    roi = np.load(data_path('roi.npy'))

    # constraints: vertex index -> vertex offset (w.r.t registration)
    point_constraints = {
        392: [0, 0.003, -0.005]       # comment to disable
    }

    constraint_keys = sorted(point_constraints.keys())
    assert all(k not in roi for k in constraint_keys), "constrained vertex already in roi"

    constraint_offsets = np.array(
        [point_constraints[k] for k in constraint_keys])

    # add constraint
    roi = np.concatenate((roi, np.array(constraint_keys)))

    # constraint matrix
    roi_matrix = assemble_roi_matrix(roi, vertex_count=len(reference.vertices))
    J = expand(roi_matrix, 3)

    assert J.shape[1] == 3 * len(reference.vertices)
    assert J.shape[0] == 3 * len(roi)

    c = J.dot(registration.vertices.flatten())

    # solve least-squares
    x = solve_constrained_least_squares(A, b, J, c)

    # display solution mesh
    solved = Mesh.create('solved',
                         vertices=x.reshape(reference.vertices.shape),
                         faces=reference.faces)

    if point_constraints:
        # apply point constraint offsets
        c[-3 * len(point_constraints):] += constraint_offsets.flatten()

        # solve least-squares with offsets
        x = solve_constrained_least_squares(A, b, J, c)

        # display solution w/point constraints mesh
        solved_points = Mesh.create('solved-points',
                                    vertices=x.reshape(reference.vertices.shape),
                                    faces=reference.faces)

        # display constrained points
        handles = ps.register_point_cloud("handles",
                                          c[-3 * len(point_constraints):].reshape((len(point_constraints), 3)))
    
    ps.show()
