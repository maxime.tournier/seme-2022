# clone 

```sh
git clone https://gitlab.com/maxime.tournier/seme-2022.git
```

# python virtual environment setup

```sh
cd seme-2020-anatoscope
python -m venv .
source bin/activate
python -m pip install -r requirements.txt
```

# run

```sh
source bin/activate # if not already in virtual env
python main.py
```




